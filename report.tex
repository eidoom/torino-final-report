\documentclass[10pt, a4paper]{article}
\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage{mathtools}
\usepackage[capitalize]{cleveref}
\usepackage[nolist]{acronym}
\usepackage{graphicx}

\setcounter{secnumdepth}{0} % suppress all section numbers

\title{Final activity report}

\author{Ryan Moodie}

\date{26 September 2023}

\begin{document}

\begin{acronym}
    \acro{LHC}{Large Hadron Collider}
    \acro{N3LO}[N\textsuperscript{3}LO]{next-to-next-to-next-to-leading order}
    \acro{NNLO}{next-to-next-to-leading order}
    \acro{QCD}{quantum chromodynamics}
    \acro{QED}{quantum electrodynamics}
\end{acronym}

\maketitle

\begin{abstract}
    Since starting here in May 2022, I have worked in Simon Badger's group on the calculation of higher-order corrections in \ac{QCD} and \ac{QED} to Standard Model amplitudes relevant for precision phenomenology experiments such as those at the \ac{LHC}.
    I have focussed on using novel technologies such as analytic reconstruction over finite fields and machine learning to perform state-of-the-art computations.
\end{abstract}

\section{Main projects}
\label{sec:projects}

\subsection{Full-colour \acs{NNLO} \acs{QCD} corrections to \texorpdfstring{$pp\to\gamma jj$}{pp -> yjj}}
We computed the \ac{NNLO} \ac{QCD} corrections to the production of an isolated photon in association with a pair of jets at hadron colliders, $pp\to\gamma jj$.
My contribution was to the full-colour calculation of the double-virtual corrections, which are massless five-particle two-loop amplitudes.
We used reconstruction over finite fields~\cite{Badger:2021imn} to obtain analytic expressions for the rational coefficients, and used the pentagon functions~\cite{Chicherin:2021dyp} for a special function basis.
Our collaborators used these amplitude ingredients to compute \ac{NNLO} distributions to demonstrate their importance for precision phenomenology.

\subsection{\texorpdfstring{$0\to\ell\bar\ell\gamma\gamma^*$}{0 -> llyy*} at two loops in massless \acs{QED}}
We calculated the \ac{N3LO} real-double-virtual \ac{QED} corrections to the decay of an off-shell photon to a lepton pair, $\gamma^*\to\ell\bar\ell$, in the approximation of massless leptons.
This includes the two-loop amplitudes for $0\to\ell\bar\ell\gamma\gamma^*$, a four-particle process with one off-shell external leg.
Having found a set of master integrals via the Laporta algorithm, we solved them using the method of differential equations and expressed them in terms of multiple polylogarithms~\cite{Badger:2023eqz}.
We again used reconstruction over finite fields for the rational coefficients.
These amplitudes are being used by the McMule collaboration for predictions relevant for the future MUonE experiment~\cite{Abbiendi:2016xup}.

\subsection{Feynman integral neural networks}

Feynman integrals form an essential part of our mathematical description of loop-level scattering amplitudes.
A family of Feynman integrals can be described by a system of partial differential equations.
Many methods exist to solve such systems, but they are limited by personnel or computational time, or even fail for certain families.
We are exploring a new approach, in which neural networks are trained to solve the differential equations using the physics-informed deep learning framework~\cite{raissi2019physics}.
This offers a flexible technique which can solve systems whose analytic solutions are elusive with little human or computer resource requirements, although at the cost of precision compared to established strategies.
We will publish our work in the near future.

\section{Talks}
\label{sec:talks}

\begin{description}
    \item[9 Jun '22] I gave a seminar titled \textit{Optimising hadron collider simulations using amplitude neural networks}~\cite{moodie_ryan_2022_7759561} to the Theoretical Physics II group at the Julius-Maximilians-Universität in Würzburg, Germany.

    \item[3--5 Aug '22] I attended the \textit{4\textsuperscript{th} Workstop/Thinkstart: Towards \acs{N3LO} for $\gamma^*\to\ell\bar\ell$} in Durham, UK.
        I gave a talk titled \textit{Analytic two-loop amplitudes with finite fields}~\cite{moodie_ryan_2022_7759592}.

    \item[20--22 Sep '22] I attended the \textit{8\textsuperscript{th} International Workshop on High Precision for Hard Processes at the \acs{LHC}} (HP$^2$ 2022) in Newcastle, UK.
        I gave a talk titled \textit{Two-loop five-point amplitudes in massless \acs{QCD} with finite fields}~\cite{moodie_ryan_2022_7759875}.

    \item[23--28 Oct '22] I attended the \textit{21\textsuperscript{st} International Workshop on Advanced Computing and Analysis Techniques in Physics Research} (ACAT 2022) in Bari, Italy.
        I gave a talk titled \textit{Two-loop five-point amplitudes in massless \acs{QCD} with finite fields}~\cite{moodie_ryan_2022_7760420}.

    \item[7 Nov '22] I gave a virtual seminar titled \textit{Optimising hadron collider simulations using matrix element neural networks}~\cite{moodie_ryan_2022_7760525} at a CMS Physics Generators meeting in CERN.

    \item[7--9 Jun '23] I virtually attended the conference \textit{Radiative corrections and Monte Carlo tools for low-energy hadronic cross sections in $e^+ e^-$ collisions} in Zurich, Switzerland.
        I gave a talk titled \textit{$e^+e^-\to\gamma\gamma^*$ at two loops in massless QED}~\cite{moodie_ryan_2023_8019969}.
\end{description}

\section{Papers}
\label{sec:papers}

\begin{description}
    \item[25 Oct '22] My PhD thesis was accepted~\cite{Moodie:2022dxs}.
    \item[1 Feb '23] A proceedings contribution for my talk at ACAT 2021 was published~\cite{Moodie:2022flt}.
    \item[17 Feb '23] I submitted a proceedings contribution for my talk at ACAT 2022 and released a preprint~\cite{Moodie:2023pql}.
    \item[13 Apr '23] We submitted an article about our $pp\to\gamma jj$ project to the \textit{Journal of High Energy Physics} (JHEP) and released a preprint~\cite{Badger:2023mgf}.
    \item[6 Jul '23] We submitted an article about our $0\to\ell\bar\ell\gamma\gamma^*$ project to JHEP and released a preprint~\cite{Badger:2023xtl}.
\end{description}

\bibliographystyle{JHEPedit}
\bibliography{bibliography}

\section{Signature}
\includegraphics{signature}

\end{document}
